# TP 201: Practical Networked Applications in Rust

## About

This is my implementation of PingCAP's [TP 201: Practical Networked Applications in Rust](https://github.com/pingcap/talent-plan/blob/master/courses/rust/README.md) course. 

### Implementation

I made a few customizations to the course:
* For error handling in the library, I used the `thiserror` crate instead of the deprecated `failure` crate.
* For serialization, I used `bincode`, due to it's speed in serialization and deserialization.
