// use serde::ser::{Serialize, SerializeStruct, Serializer};
// // use serde::{Deserialize, Serialize};

use ron;
use ron::Result;
use serde::{Deserialize, Serialize};
// use serde_json::Result;
use bincode::serialize_into;
use bson::{from_document, to_document, Bson, Document};
use std::error::Error;
use std::fs::File;
use std::io::{BufReader, BufWriter, Read, Write};

#[derive(Serialize, Deserialize, Debug)]
struct Move {
    row: i32,
    col: i32,
}

fn main() {
    // exercise1();
    // exercise2_1();
    // exercise2_2();
    // exercise3_1();
    // exercise3_2().unwrap();
    // exercise3_3();
    exercise4();
}

fn exercise1() -> std::result::Result<(), Box<dyn Error>> {
    println!("Implement Exercise 1: Serialize and deserialize a data structure with serde (JSON).");
    let a = Move { row: 3, col: 9 };
    let mut file = File::create("test.txt")?;
    file.write_all(serde_json::to_string(&a)?.as_ref())?;

    file = File::open("test.txt")?;
    let mut content = String::new();
    file.read_to_string(&mut content)?;
    let b: Move = serde_json::from_str(content.as_str())?;
    println!("{:?}", a);
    println!("{:?}", b);

    println!();
    Ok(())
}
fn exercise2_1() -> std::result::Result<(), Box<dyn Error>> {
    println!("Implement Exercise 2 Part 1: Serialize and deserialize a data structure to a buffer with serde (JSON)");
    let a = Move { row: 3, col: 9 };

    println!("To Serialize: {:?}", a);
    let v = serde_json::to_vec(&a)?;
    let s = std::str::from_utf8(&v)?;
    println!("Serialized Version: {:?}", s);

    let b: Move = serde_json::from_str(s)?;
    println!("After Deserializing: {:?}", b);

    println!();
    Ok(())
}

fn exercise2_2() -> std::result::Result<(), Box<dyn Error>> {
    println!("Implement Exercise 2 Part 2: Serialize and deserialize a data structure to a buffer with serde (RON).");
    let a = Move { row: 3, col: 9 };
    println!("To Serialize: {:?}", a);

    let s = ron::ser::to_string(&a)?;
    println!("Serialized Version: {:?}", s);

    let b: Move = ron::de::from_str(&s)?;
    println!("After Deserializing: {:?}", b);

    println!();
    Ok(())
}

fn exercise3_1() -> std::result::Result<(), Box<dyn Error>> {
    println!("Implement Exercise 3 Part 1: Serialize and deserialize 1000 data structures with serde (BSON) to File.");

    let mut file = File::create("test.txt")?;
    let mut v = vec![];
    for i in 0..100 {
        v.push(Move { row: i, col: i });
    }
    println!("{:?}", v);
    file.write_all(ron::to_string(&v)?.as_ref())?;

    file = File::open("test.txt")?;
    let mut content = String::new();
    file.read_to_string(&mut content)?;
    let b: Vec<Move> = ron::from_str(content.as_str())?;
    println!("{:?}", b);
    println!();
    Ok(())
}

fn exercise3_2() -> std::result::Result<(), Box<dyn Error>> {
    println!("Implement Exercise 3 Part 2: Serialize and deserialize 1000 data structures with serde (BSON) to File.\nOne at a time.");

    let mut file = File::create("test.bson")?;
    let mut doc = Document::new();

    for i in 1..10 {
        let a = Move { row: i, col: i };
        let a_bson = bson::to_bson(&a)?;
        doc.insert(format!("move:{}", i), a_bson);
    }

    let doc: Document = bson::from_document(doc)?;
    let mut iter = doc.iter();
    while let Some(inside_doc) = iter.next() {
        let (k, v) = inside_doc;
        let move_val: Move = bson::from_bson(v.to_owned())?;
        println!("{:?}", move_val);
    }

    Ok(())
}

fn exercise3_3() -> std::result::Result<(), Box<dyn Error>> {
    println!("Implement Exercise 3 Part 3: Serialize and deserialize 1000 data structures with serde (BSON) to File.");

    println!();
    Ok(())
}

#[derive(Serialize, Deserialize, Debug)]
struct testTuple(String, String);

fn exercise4() -> std::result::Result<(), Box<dyn Error>> {
    /// Verify that I am using bincode correctly by:
    /// Serializing a tuple to a file and deserializing it
    /// and making sure that the before and after values are the same.
    let mut f = File::create("test.bin").unwrap();
    let kv = testTuple("1 key".to_string(), "2 value".to_string());
    // let encoded_kv = bincode::serialize(&kv).unwrap();
    serialize_into(&mut f, &kv).unwrap();
    println!("kv before: {:?}", kv);
    let f = File::open("test.bin").unwrap();
    let mut reader = BufReader::new(f);
    let kv: testTuple = bincode::deserialize_from(&mut reader).unwrap();
    println!("kv after: {:?}", kv);
    println!();
    Ok(())
}
