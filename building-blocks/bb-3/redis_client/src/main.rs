use std::io::prelude::*;
use std::net::TcpStream;
use std::str;

fn main() {
    let mut stream = TcpStream::connect("127.0.0.1:6379").unwrap();

    let msg = b"PING\r\n";

    stream.write(msg).unwrap();

    let mut buffer = [0u8; 1024];
    stream.read(&mut buffer).unwrap();

    println!("Received contents: {}", str::from_utf8(&buffer).unwrap());
}
