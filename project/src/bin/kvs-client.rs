extern crate clap;
use anyhow::Result;
use kvs::KvStore;
use std::path::Path;
use std::process::exit;
use structopt::StructOpt;

use std::env;

#[derive(StructOpt, Debug)]
#[structopt(author, about)]
enum Opt {
    #[structopt(name = "set", about = "Sets the value of a string key to a string")]
    Set {
        #[structopt(value_name = "KEY", help = "Sets the string key")]
        key: String,
        #[structopt(value_name = "VALUE", help = "Sets the value of the given key")]
        value: String,
    },
    #[structopt(name = "get", about = "Get the string value of a given string key")]
    Get {
        #[structopt(
            value_name = "KEY",
            help = "Gets the value associated with the given key"
        )]
        key: String,
    },
    #[structopt(name = "rm", about = "Remove a given key")]
    Rm {
        #[structopt(
            value_name = "KEY",
            help = "Removes the value associated with the given key"
        )]
        key: String,
    },
}

fn main() -> Result<()> {
    let path = env::current_dir()?;
    let mut kvstore = KvStore::open(Path::new(&path)).unwrap();

    match Opt::from_args() {
        Opt::Set { key, value } => match kvstore.set(key, value) {
            Err(_) => {
                eprintln!("Error while setting key");
                exit(1);
            }
            Ok(_) => exit(0), // Ok(()) => Ok(()),
        },
        Opt::Get { key } => match kvstore.get(key) {
            Err(_) => {
                eprintln!("Error while getting key.");
                exit(1);
            }
            Ok(key) => match key {
                Some(val) => {
                    println!("{}", val);
                    exit(0);
                }
                None => {
                    println!("Key not found");
                    exit(0);
                }
            },
        },
        Opt::Rm { key } => match kvstore.remove(key) {
            Err(_) => {
                println!("Key not found");
                exit(1);
            }
            Ok(_) => exit(0),
        },
        _ => unreachable!(),
    }
}
