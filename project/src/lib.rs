use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs;
use std::fs::{File, OpenOptions};
use std::io::prelude::*;
use std::io::{BufReader, BufWriter, SeekFrom};
use std::path::{Path, PathBuf};
use tempfile::TempDir;
use thiserror::Error;

pub type Result<T> = anyhow::Result<T, KvsError>;

#[derive(Error, Debug)]
pub enum KvsError {
    /// Represents a failure when attempting to serialize to bincode
    #[error("Kv deserialization error")]
    KvDeserializationError { source: bincode::Error },
    #[error("Kv serialization error")]
    KvSerializationError { source: bincode::Error },
    #[error("File creation error")]
    FileCreationError { source: std::io::Error },
    #[error("File read error")]
    FileReadError { source: std::io::Error },
    #[error("Seek error")]
    FileSeekError { source: std::io::Error },
    #[error("Key not found error")]
    KeyNotFoundError,
    #[error("Kv could not be written to file")]
    FileWriteError { source: std::io::Error },
    #[error("Temporary file could not be created for compaction")]
    TemporaryFileCompactionError { source: std::io::Error },
    #[error("Original file couldn't be overwritten with compacted file")]
    CompactionError { source: std::io::Error },
}
/// The `KvStore` stores string key-value pairs in a hashmap
///
/// Example:
///
/// ```rust
/// # use kvs::KvStore;
/// # use tempfile::TempDir;
///
/// let temp_dir = TempDir::new().expect("unable to create temporary working directory");
/// let mut store = KvStore::open(temp_dir.path()).unwrap();
/// assert_eq!(store.get("test_key".to_owned()).unwrap(), None);
/// store.set("key1".to_owned(), "val1".to_owned()).unwrap();
/// assert_eq!(store.get("key1".to_owned()).unwrap(), Some("val1".to_owned()));
/// assert!(store.remove("key1".to_owned()).is_ok());
/// assert_eq!(store.get("key1".to_owned()).unwrap(),None);
/// ```

pub struct KvsEngine {}

/// The writer stays at the end of the file at all times
pub struct KvStore {
    map: HashMap<String, u64>,
    path: PathBuf,
    writer: BufWriter<File>,
    garbage_count: u64,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct KV(String, String);

impl KvStore {
    /// Creates a new `KvStore`
    pub fn new(path: PathBuf) -> Result<Self> {
        Ok(KvStore::open(&path)?)
    }

    /// Adds a new key-value pair to the key-value store
    ///
    /// If the key already exists the value will be overwritten
    pub fn set(&mut self, key: String, value: String) -> Result<()> {
        let pos = self
            .writer
            .seek(SeekFrom::Current(0))
            .map_err(|source| KvsError::FileSeekError { source })?;

        if self.map.contains_key(&key) {
            self.garbage_count += 1;
            if self.garbage_count > 1000 {
                self.compact()?;
            }
        }

        self.map.insert(key.clone(), pos);

        let kv = KV(key.clone(), value.clone());
        bincode::serialize_into(&mut self.writer, &kv)
            .map_err(|source| KvsError::KvSerializationError { source })?;
        self.writer
            .flush()
            .map_err(|source| KvsError::FileWriteError { source })?;

        Ok(())
    }

    /// Gets a value associated with the key
    ///
    /// Returns `None` if the key does not exist in the key-value store
    pub fn get(&self, key: String) -> Result<Option<String>> {
        match self.map.get(&key) {
            Some(i) => {
                let mut f = File::open(Path::new(&self.path))
                    .map_err(|source| KvsError::FileReadError { source })?;
                f.seek(SeekFrom::Start(*i))
                    .map_err(|source| KvsError::FileSeekError { source })?;
                let mut reader = BufReader::new(f);
                let KV(_, val) = bincode::deserialize_from(&mut reader)
                    .map_err(|source| KvsError::KvDeserializationError { source })?;
                Ok(Some(val))
            }
            _ => Ok(None),
        }
    }

    /// Removes the key from the key-value store
    /// Writes "_" to signify that it is not in the log
    pub fn remove(&mut self, key: String) -> Result<()> {
        match self.map.get(&key) {
            Some(_) => {
                let kv = KV(key.clone(), "_".to_string());
                bincode::serialize_into(&mut self.writer, &kv)
                    .map_err(|source| KvsError::KvSerializationError { source })?;
                self.map.remove(&key);
                self.writer
                    .flush()
                    .map_err(|source| KvsError::FileWriteError { source })?;
                self.garbage_count += 1;

                Ok(())
            }
            _ => Err(KvsError::KeyNotFoundError),
        }
    }
    /// Opens the path specified.
    /// If the path doesn't exist, it will create it.
    pub fn open(path: &Path) -> Result<KvStore> {
        let path = &path.join("kvs.bin");

        let f;
        if path.exists() {
            f = File::open(path).map_err(|source| KvsError::FileReadError { source })?;
        } else {
            f = File::create(path).map_err(|source| KvsError::FileCreationError { source })?;
        }

        let mut m = HashMap::new();
        let mut reader = BufReader::new(f);

        // Generate hashmap by looping through the file
        // while counting the total number of possible compactions `garbage_count`
        let mut garbage_count = 0;
        loop {
            let pos = reader
                .seek(SeekFrom::Current(0))
                .map_err(|source| KvsError::FileSeekError { source })?;

            match bincode::deserialize_from(&mut reader) {
                Ok(p) => {
                    let KV(key, val) = p;
                    if let "_" = &val[..] {
                        garbage_count += 1;
                        m.remove(&key);
                        continue;
                    } else {
                        if m.contains_key(&key) {
                            garbage_count += 1;
                        }
                        m.insert(key.to_owned(), pos);
                    }
                }
                _ => break,
            }
        }

        let mut f = OpenOptions::new()
            .read(false)
            .write(true)
            .create(true)
            .open(path)
            .map_err(|source| KvsError::FileReadError { source })?;

        f.seek(SeekFrom::End(0))
            .map_err(|source| KvsError::FileSeekError { source })?;

        let writer = BufWriter::new(f);

        let mut store = KvStore {
            map: m,
            path: path.to_path_buf(),
            writer,
            garbage_count,
        };

        if garbage_count > 0 {
            store.compact()?;
        }

        Ok(store)
    }

    pub fn compact(&mut self) -> Result<()> {
        let temp_dir =
            TempDir::new().map_err(|source| KvsError::TemporaryFileCompactionError { source })?;

        let mut store = KvStore::open(&temp_dir.path())?;
        for (key, _) in &self.map {
            let value = self.get(key.to_owned())?;
            match value {
                Some(val) => store.set(key.to_owned(), val.to_owned())?,
                _ => continue,
            }
        }
        // Overwrite the old file
        fs::copy(&store.path, &self.path).map_err(|source| KvsError::CompactionError { source })?;
        // Temp file deletion shouldn't be necessary because it will automatically be deleted once out of scope
        // fs::remove_file(&store.path)?;

        let mut f = OpenOptions::new()
            .read(false)
            .write(true)
            .create(true)
            .open(Path::new(&self.path))
            .map_err(|source| KvsError::FileReadError { source })?;

        f.seek(SeekFrom::End(0))
            .map_err(|source| KvsError::FileSeekError { source })?;

        let writer = BufWriter::new(f);

        // The writer needs to be pointed to the correct file instead of the temp file
        self.writer = writer;
        self.map = store.map;
        self.garbage_count = 0;
        Ok(())
    }
}
